package au.com.ofs.psr.appcore;

import java.util.Comparator;
import java.util.Random;

/**
 * Created by Eran on 12/03/2017.
 */
public class MoveCalculator {
    private String[] possibleMoves = new String[]{"P", "S", "R"};
    public String calculateComputerNextMove(String userMove) {
        String nextMove = possibleMoves[0];
        Random random = new Random(System.currentTimeMillis());
        int nextMoveIdx = random.nextInt(3);

        nextMove = possibleMoves[nextMoveIdx];

        return nextMove;
    }

    public int calculateWinner(String party1, String party2) {
        MoveComparator moveComparator = new MoveComparator();
        int retVal = moveComparator.compare(party1, party2);

        return retVal;
    }

    /**
     * The class compares the two parties' moves according to the Rock Paper Scissors game rules and returns:
     * 0  - in case of a tie.
     * -1 - in the case that party one loses.
     * 1  - in the case that party one wins.
     */
    private class MoveComparator implements Comparator<String> {

        @Override
        public int compare(String party1Move, String party2Move) {
            int retValue = 0;
            if (party1Move.equals(party2Move) == true){
                retValue = 0;
            } else if (party1Move.equals("P") == true) {
                if (party2Move.equals("S") == true) {
                    retValue = -1;
                } else {
                    retValue = 1;
                }
            } else if (party1Move.equals("S") == true) {
                if (party2Move.equals("R") == true) {
                    retValue = -1;
                } else {
                    retValue = 1;
                }
            } else if (party1Move.equals("R") == true) {
                if (party2Move.equals("P") == true) {
                    retValue = -1;
                } else {
                    retValue = 1;
                }
            }

            return retValue;
        }
    }
}
