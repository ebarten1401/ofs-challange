package au.com.ofs.psr.appcore.dto;

/**
 * Created by Eran on 11/03/2017.
 */
public class CredentialsDto {

    private String userName;

    private String password;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
