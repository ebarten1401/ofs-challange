package au.com.ofs.psr.appcore.webserver;

import au.com.ofs.psr.appcore.dao.CredentialsDao;
import au.com.ofs.psr.appcore.filter.AuthenticationFilter;
import au.com.ofs.psr.appcore.filter.CacheAgeFilter;
import au.com.ofs.psr.appcore.service.PsrService;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.ScanningHibernateBundle;
import io.dropwizard.servlets.CacheBustingFilter;
import io.dropwizard.servlets.SlowRequestFilter;
import io.dropwizard.servlets.ThreadNameFilter;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.util.Duration;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.DispatcherType;
import java.util.*;

/**
 * Created by Eran on 11/03/2017.
 */
public class WebContainerApplication<C extends WebConfiguration> extends Application<C> {

    private static final Logger _logger = LoggerFactory.getLogger(WebContainerApplication.class);


    @Override
    public String getName() {
        return "PSR Game";
    }

    @Override
    public void initialize(Bootstrap<C> configurationBootstrap) {

        //adding asset bundle which will hold the frontend files
        AssetsBundle assetsBundle = new AssetsBundle("/assets", "/", "index.html");
        configurationBootstrap.addBundle(assetsBundle);

        //adding hibernate bundle to talk to the database
        configurationBootstrap.addBundle(_hibernateBundle);

    }

    @Override
    public void run(C configuration, Environment environment) throws Exception {

        //will read specific configuration from the YAML and initialize the server accordingly
        initializeServer(configuration, environment);

    }


    /**
     * the method initializes the server with specific configuration read from the YAML file
     *
     * @param configuration The specific configuration to initialize the server with.
     * @param environment   The environment used to initialize the server.
     */
    protected void initializeServer(C configuration, Environment environment) throws Exception {

        SessionFactory sessionFactory = _hibernateBundle.getSessionFactory();
        CredentialsDao credentialsDao = new CredentialsDao(sessionFactory);
        PsrService psrService = new PsrService(sessionFactory, credentialsDao);

        //registering the endpoint
        environment.jersey().register(psrService);

        environment.servlets().addFilter("AuthenticationFilter", new AuthenticationFilter())
                .addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), true, "/*");

        // get the cache control settings from the YAML - configuration
        CacheControl cacheControl = configuration.getCacheControl();
        if(cacheControl != null){
            String cacheType = cacheControl.getCacheType();
            int maxAge = cacheControl.getMaxAge();

            if (cacheType.equals("nocache")) {
                // caching was enabled in YAML - was set to true - enabling the cacheBustingFilter
                // this will ALWAYS return  "must-revalidate,no-cache,no-store" in the Cache-Control response header

                environment.servlets().addFilter("CacheBustingFilter", new CacheBustingFilter())
                        .addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), true, "/*");
            }
            else if(cacheType.equals("smart")){
                //sending only "max-age=0" as the response header.
                environment.servlets().addFilter("CacheAgeFilter", new CacheAgeFilter(maxAge))
                        .addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), true, "/*");
            }
            else if(cacheType.equals("nocontrol")){
                // do nothing
            }
        }
        else{
            // if there is no settings in the yml send a default header with max-age as 5 minutes
            //sending only "max-age=5" as the response header.
            _logger.warn("No cache settings found. max-age will be set to 10 minutes");
            environment.servlets().addFilter("CacheAgeFilter", new CacheAgeFilter(600))
                    .addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), true, "/*");
        }


        //This will log any requests that take longer than 10 seconds
        Duration duration = Duration.seconds(10L);
        environment.servlets().addFilter("SlowRequestFilter", new SlowRequestFilter(duration))
                    .addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), true, "/*");
        environment.servlets().addFilter("ThreadNameFilter", new ThreadNameFilter())
                    .addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), true, "/*");

    }


    /**
     * Internal hibernate bundle to store the requests - this will act as queue to store critical requests which would be
     * triggered again in the event of a server crash
     */
    private final ScanningHibernateBundle<WebConfiguration> _hibernateBundle = new ScanningHibernateBundle<WebConfiguration>("au.com.ofs.psr.appcore.entity") {
        @Override
        public DataSourceFactory getDataSourceFactory(WebConfiguration configuration) {
            return configuration.getDataSourceFactory();
        }
    };

    public static void main(String[] args) throws Exception {
        new WebContainerApplication<>().run(args);

    }
}
