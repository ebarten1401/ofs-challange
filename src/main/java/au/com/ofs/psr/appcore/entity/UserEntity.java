package au.com.ofs.psr.appcore.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Eran on 12/03/2017.
 */
@Entity
@Table(name="psr_user")
public class UserEntity implements Serializable {

    @Id
    @Column(name = "\"userId\"")
    private String userId;

    @Column(name="\"name\"")
    private String name;

    @Column(name = "\"winStreak\"")
    private int winStreak;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getWinStreak() {
        return winStreak;
    }

    public void setWinStreak(int winStreak) {
        this.winStreak = winStreak;
    }
}
