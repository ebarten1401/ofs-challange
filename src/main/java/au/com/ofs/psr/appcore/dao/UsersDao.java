package au.com.ofs.psr.appcore.dao;

import au.com.ofs.psr.appcore.entity.UserEntity;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

/**
 * Created by Eran on 12/03/2017.
 */
public class UsersDao extends AbstractDAO<UserEntity> {

    /**
     * Creates a new DAO with a given session provider.
     *
     * @param sessionFactory a session provider
     */
    public UsersDao(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public UserEntity findById(String userId){
        return get(userId);
    }

    public void insert(UserEntity userEntity) {
        currentSession().save(userEntity);
    }

    public void update(UserEntity userEntity) {
        currentSession().update(userEntity);
    }
}
