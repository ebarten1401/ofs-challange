package au.com.ofs.psr.appcore.filter;

import javax.servlet.*;
import java.io.IOException;

/**
 * Created by Eran on 11/03/2017.
 */
public class AuthenticationFilter implements Filter {


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        chain.doFilter(request, response);

    }

    @Override
    public void destroy() {

    }
}
