package au.com.ofs.psr.appcore.webserver;

/**
 * Created by Eran on 11/03/2017.
 */
public class CacheControl {

        private String cacheType;

        private int maxAge;

        public CacheControl(){}

        public CacheControl(String cacheType, int maxAge) {
            this.cacheType = cacheType.toLowerCase();
            this.maxAge = maxAge;
        }

        public String getCacheType() {
            return cacheType;
        }

        public void setCacheType(String cacheType) {
            this.cacheType = cacheType.toLowerCase();
        }

        public int getMaxAge() {
                return maxAge;
            }

        public void setMaxAge(int maxAge) {
            this.maxAge = maxAge;
        }

}
