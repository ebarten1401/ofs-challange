package au.com.ofs.psr.appcore.webserver;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;

/**
 * Created by Eran on 11/03/2017.
 * To get custom configuration from psr.yml for this web server
 */
public class WebConfiguration extends Configuration {

    @JsonProperty
    private CacheControl cacheControl;

    @JsonProperty
    private DataSourceFactory database = new DataSourceFactory();

    @JsonProperty("database")
    public DataSourceFactory getDataSourceFactory() {
        return database;
    }

    public CacheControl getCacheControl() {
        return cacheControl;
    }

    public void setCacheControl(CacheControl cacheControl) {
        this.cacheControl = cacheControl;
    }

}
