package au.com.ofs.psr.appcore.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Eran on 12/03/2017.
 */
@Entity
@Table(name = "GamePlayerData")
public class GamePlayerDataEntity implements Serializable {

    @Id
    @Column(name = "\"gameId\"")
    private long gameId;

    @Id
    @Column(name = "\"playerId\"")
    private String playerId;

    @Column(name="\"choice\"")
    private String choice;


    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public String getChoice() {
        return choice;
    }

    public void setChoice(String choice) {
        this.choice = choice;
    }

    public long getGameId() {
        return gameId;
    }

    public void setGameId(long gameId) {
        this.gameId = gameId;
    }

}
