package au.com.ofs.psr.appcore.dao;

import au.com.ofs.psr.appcore.entity.CredentialsEntity;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

/**
 * Created by Eran on 12/03/2017.
 */
public class CredentialsDao extends AbstractDAO<CredentialsEntity> {

    /**
     * Creates a new DAO with a given session provider.
     *
     * @param sessionFactory a session provider
     */
    public CredentialsDao(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public CredentialsEntity findById(String userId){
        return get(userId);
    }

    public void insert(CredentialsEntity credentialsEntity) {
        currentSession().save(credentialsEntity);
    }
}
