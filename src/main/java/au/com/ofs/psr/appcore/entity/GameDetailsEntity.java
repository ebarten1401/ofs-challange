package au.com.ofs.psr.appcore.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Eran on 12/03/2017.
 */
@Entity
@Table(name="GameDetails")
public class GameDetailsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="\"gameId\"")
    private long gameId;

    @Column(name="\"winnerUser\"")
    private String winnerUser;

    @Column(name="\"date\"")
    private Date date;

    public long getGameId() {
        return gameId;
    }

    public void setGameId(long gameId) {
        this.gameId = gameId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getWinnerUser() {
        return winnerUser;
    }

    public void setWinnerUser(String winnerUser) {
        this.winnerUser = winnerUser;
    }
}
