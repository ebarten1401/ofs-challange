package au.com.ofs.psr.appcore.dto;

/**
 * Created by Eran on 12/03/2017.
 */
public class UserPlayDto {
    private String userName;
    private String move;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMove() {
        return move;
    }

    public void setMove(String move) {
        this.move = move;
    }
}
