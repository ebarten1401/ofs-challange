package au.com.ofs.psr.appcore.service;

import au.com.ofs.psr.appcore.MoveCalculator;
import au.com.ofs.psr.appcore.dao.CredentialsDao;
import au.com.ofs.psr.appcore.dao.GameDetailsDao;
import au.com.ofs.psr.appcore.dao.GamePlayerDataDao;
import au.com.ofs.psr.appcore.dao.UsersDao;
import au.com.ofs.psr.appcore.dto.CredentialsDto;
import au.com.ofs.psr.appcore.dto.GameResultDto;
import au.com.ofs.psr.appcore.dto.UserPlayDto;
import au.com.ofs.psr.appcore.entity.CredentialsEntity;
import au.com.ofs.psr.appcore.entity.GameDetailsEntity;
import au.com.ofs.psr.appcore.entity.GamePlayerDataEntity;
import au.com.ofs.psr.appcore.entity.UserEntity;
import io.dropwizard.hibernate.UnitOfWork;
import org.hibernate.SessionFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;

/**
 * Created by Eran on 12/03/2017.
 */
@Path("/psr")
public class PsrService {

    private GamePlayerDataDao _gamePlayerDataDao;
    private SessionFactory _sessionFactory;
    private CredentialsDao _credentialsDao;
    private UsersDao _usersDao;
    private GameDetailsDao _gameDetailsDao;

    public PsrService(SessionFactory sessionFactory, CredentialsDao credentialsDao){
        _sessionFactory = sessionFactory;
        _credentialsDao = credentialsDao;
        _usersDao = new UsersDao(_sessionFactory);
        _gameDetailsDao = new GameDetailsDao(sessionFactory);
        _gamePlayerDataDao = new GamePlayerDataDao(sessionFactory);
    }

    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @UnitOfWork
    public Response login(@Context HttpHeaders httpHeaders, CredentialsDto credentialsDto) {
        Response response = null;
        String password = credentialsDto.getPassword();
        String userName = credentialsDto.getUserName();
        CredentialsEntity existingCredentials = _credentialsDao.findById(userName);
        if(existingCredentials != null) {
            String existingPassword = existingCredentials.getPassword();
            if (existingPassword.equals(password) == true) {
                response = Response.status(Response.Status.OK).build();
            } else {
                response = Response.status(Response.Status.UNAUTHORIZED).build();
            }
        } else{
            response = Response.status(Response.Status.UNAUTHORIZED).build();
        }
        return response;
    }

    @POST
    @Path("/createAccount")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @UnitOfWork
    public Response createAccount(@Context HttpHeaders httpHeaders, CredentialsDto credentialsDto) {
        Response response = null;
        String password = credentialsDto.getPassword();
        String userName = credentialsDto.getUserName();
        CredentialsEntity existingCredentials = _credentialsDao.findById(userName);
        if(existingCredentials == null) {
            CredentialsEntity credentialsEntity = new CredentialsEntity(userName, password);
            _credentialsDao.insert(credentialsEntity);
            UserEntity userEntity = new UserEntity();
            userEntity.setName("");
            userEntity.setUserId(userName);
            _usersDao.insert(userEntity);
            response = Response.status(Response.Status.OK).build();
        } else{
            response = Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
        }
        return response;
    }

    @POST
    @Path("/userPlay")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @UnitOfWork
    public Response userPlay(@Context HttpHeaders httpHeaders, UserPlayDto userPlayDto) {
        String userName = userPlayDto.getUserName();
        String userMove = userPlayDto.getMove();

        // Get computer move
        MoveCalculator moveCalculator = new MoveCalculator();
        String computerMove = moveCalculator.calculateComputerNextMove(userMove);

        // Calculate winner
        int gameResult = moveCalculator.calculateWinner(userMove, computerMove);

        // Calculate game result
        GameResultDto gameResultDto = new GameResultDto(gameResult, computerMove);

        // Save current game's data.
        storeCurrentGameDetails(userName, userMove, computerMove, gameResult);

        // Find current user's winning streaks
        UserEntity userEntity = _usersDao.findById(userName);
        int winStreak = userEntity.getWinStreak();
        gameResultDto.setWinningStreaks(winStreak);

        Response response = Response.ok(gameResultDto).build();
        return response;
    }

    /**
     * The method stores the current game's details into the DB.
     * @param userName The name of the current user.
     * @param userMove The current user's move.
     * @param computerMove The computer's move.
     * @param gameResult The game result (who wins).
     */
    private void storeCurrentGameDetails(String userName, String userMove, String computerMove, int gameResult) {
        // Get current user's entity
        UserEntity userEntity = _usersDao.findById(userName);
        UserEntity computerEntity = _usersDao.findById("computer");
        if (computerEntity == null) {
            // If computer entity does not exist - create it to be later added into the DB.
            CredentialsEntity computerCredentials = new CredentialsEntity("computer", null);
            _credentialsDao.insert(computerCredentials);

            computerEntity = new UserEntity();
            computerEntity.setUserId("computer");
            computerEntity.setName("Computer");
            _usersDao.insert(computerEntity);
        }

        // Find out the winner UserEntity
        String winnerUser = null;
        if (gameResult > 0) {
            // Set winner user
            winnerUser = userEntity.getUserId();

            updateWinningStreaks(userEntity, computerEntity);
        } else if (gameResult < 0) {
            winnerUser = computerEntity.getUserId();

            updateWinningStreaks(computerEntity, userEntity);
        }

        // Store game data
        Date gameDate = new Date();

        // Add game data to the computer's games list
        GameDetailsEntity gameDetails = new GameDetailsEntity();
        gameDetails.setDate(gameDate);
        gameDetails.setWinnerUser(winnerUser);
        _gameDetailsDao.insert(gameDetails);

        // Update computer's game player data
        long gameId = gameDetails.getGameId();
        GamePlayerDataEntity computerGamePlayerDataEntity = new GamePlayerDataEntity();
        computerGamePlayerDataEntity.setChoice(computerMove);
        computerGamePlayerDataEntity.setGameId(gameId);
        computerGamePlayerDataEntity.setPlayerId(computerEntity.getUserId());
        _gamePlayerDataDao.insert(computerGamePlayerDataEntity);

        // Update computer's game player data
        GamePlayerDataEntity userGamePlayerDataEntity = new GamePlayerDataEntity();
        userGamePlayerDataEntity.setChoice(userMove);
        userGamePlayerDataEntity.setGameId(gameId);
        userGamePlayerDataEntity.setPlayerId(userEntity.getUserId());
        _gamePlayerDataDao.insert(userGamePlayerDataEntity);
    }

    /**
     * The method updates into the DB the winning streaks for the losing and winning entities.
     * @param winningEntity The winning entity.
     * @param losingEntity The losing entity.
     */
    private void updateWinningStreaks(UserEntity winningEntity, UserEntity losingEntity) {
        // Update winning streak for user
        int winStreak = winningEntity.getWinStreak();
        winningEntity.setWinStreak(winStreak + 1);
        _usersDao.update(winningEntity);

        // Update winning streak for computer
        losingEntity.setWinStreak(0);
        _usersDao.update(losingEntity);
    }

    @GET
    @Path("/getScoreBoard")
    @Produces(MediaType.APPLICATION_JSON)
    @UnitOfWork
    public Response getScoreBoard(@Context HttpHeaders httpHeaders){
        Response response = null;

        return response;
    }

}
