package au.com.ofs.psr.appcore.dto;

/**
 * Created by Eran on 12/03/2017.
 */
public class GameResultDto {
    private int result;
    private String opponentMove;
    private int winningStreaks;

    public GameResultDto(int result, String opponentMove) {
        this.result = result;
        this.opponentMove = opponentMove;
        this.winningStreaks = winningStreaks;
    }

    public GameResultDto() {
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public String getOpponentMove() {
        return opponentMove;
    }

    public void setOpponentMove(String opponentMove) {
        this.opponentMove = opponentMove;
    }

    public int getWinningStreaks() {
        return winningStreaks;
    }

    public void setWinningStreaks(int winningStreaks) {
        this.winningStreaks = winningStreaks;
    }
}
