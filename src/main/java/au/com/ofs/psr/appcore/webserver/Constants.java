package au.com.ofs.psr.appcore.webserver;


/**
 * The class is used as a common place holder for general constants for the application.
 */
public class Constants {

    /* Name of the HTTP Header that should have the session Id */
    public static final String AUTHORIZATION_TOKEN = "Authorization";

}

