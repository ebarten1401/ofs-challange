package au.com.ofs.psr.appcore.entity;

import javax.persistence.*;

/**
 * Created by Eran on 11/03/2017.
 */
@Entity
@Table(name="credentials")
public class CredentialsEntity {

    @Id
    @Column(name = "\"userId\"")
    private String userId;

    @Column(name = "\"pass\"")
    private String password;

    public CredentialsEntity(String userName, String password) {
        userId = userName;
        this.password = password;
    }

    public CredentialsEntity() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}