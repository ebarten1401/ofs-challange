package au.com.ofs.psr.appcore.dao;

import au.com.ofs.psr.appcore.entity.GamePlayerDataEntity;
import au.com.ofs.psr.appcore.entity.UserEntity;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

/**
 * Created by Eran on 12/03/2017.
 */
public class GamePlayerDataDao extends AbstractDAO<GamePlayerDataEntity> {

    /**
     * Creates a new DAO with a given session provider.
     *
     * @param sessionFactory a session provider
     */
    public GamePlayerDataDao(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public GamePlayerDataEntity findById(String userId){
        return get(userId);
    }

    public void insert(GamePlayerDataEntity gamePlayerDataEntity) {
        currentSession().save(gamePlayerDataEntity);
    }

    public void update(GamePlayerDataEntity gamePlayerDataEntity) {
        currentSession().update(gamePlayerDataEntity);
    }

}
