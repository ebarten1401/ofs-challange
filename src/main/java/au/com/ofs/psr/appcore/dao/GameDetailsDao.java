package au.com.ofs.psr.appcore.dao;

import au.com.ofs.psr.appcore.entity.GameDetailsEntity;
import au.com.ofs.psr.appcore.entity.GamePlayerDataEntity;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

/**
 * Created by Eran on 12/03/2017.
 */
public class GameDetailsDao extends AbstractDAO<GameDetailsEntity> {

    /**
     * Creates a new DAO with a given session provider.
     *
     * @param sessionFactory a session provider
     */
    public GameDetailsDao(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public GameDetailsEntity findById(String userId){
        return get(userId);
    }

    public void insert(GameDetailsEntity gameDetailsEntity) {
        currentSession().save(gameDetailsEntity);
    }

    public void update(GameDetailsEntity gameDetailsEntity) {
        currentSession().update(gameDetailsEntity);
    }

}
