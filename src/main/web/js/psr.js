isLogin = true;
counter = 10;
currentUser = "";
userWinningStreaks = 0;
userActionsDisabled = false;

psrImages = {
    "unknown":"images/unknown.png",
    "paper":"images/paper.png",
    "scissors":"images/scissors.png",
    "rock":"images/rock.png"
};

userGameInfo = {
    gameResult : {
    "success" : "You Win !!! How about another one?",
    "failure" : "You lost. How about another one?"
    }
};

initImages = function() {
    var paperImage = document.getElementById("paperImg");
    paperImage.src = psrImages.paper;

    var scissorsImage = document.getElementById("scissorsImg");
    scissorsImage.src = psrImages.scissors;

    var rockImage = document.getElementById("rockImg");
    rockImage.src = psrImages.rock;

    var opponentImage = document.getElementById("opponentImg");
    opponentImage.src = psrImages.unknown;
};

countdownFunction = function(){
    // Init game user info
    var userInfo = document.getElementById("gameInfo");
    var userText = "";
    if (counter > 0) {
        userText = "You have " + counter + " seconds to make your move!";
    } else {
        userText = "You Loose !!!";
        var nextGameSection = document.getElementById("nextGameSection");
        nextGameSection.className = "visible";

    }
    userInfo.innerHTML = userText;
    counter--;
};

countdownTimer = null;

submit = function(){
    var userName = document.getElementById("userName").value;
    var password = document.getElementById("password").value;
    if (isLogin) {
        login(userName, password);
    } else {
        createAccount(userName, password);
    }
};

login = function (userName, password) {
    var payload = {
        userName: userName,
        password: password
    };

    jQuery.ajax ({
        url: "api/psr/login",
        type: "POST",
        data: JSON.stringify(payload),
        contentType: "application/json; charset=utf-8",
        success: function(data, status){
            if (status === "success") {
                onSuccessfulLogin(userName);
            } else {
                updateAccountStatus("Failed to create account!")
            }
        },
        error: function (textStatus, errorThrown) {
            if (textStatus.status == "401"){
                updateAccountStatus("user does NOT exists. Please create account first.");
            } else {
                updateAccountStatus(textStatus + errorThrown);
            }

    }
});

};

onSuccessfulLogin = function(userName){
    currentUser = userName;

    // Hide login section
    var userAccountSection = document.getElementById("accountSection");
    userAccountSection.className = "hidden";

    // Init Game section images
    initImages();

    /**
     * Init Game section
     */
    counter = getCountdownTime();

    countdownTimer = setInterval (countdownFunction, 1000);

    // Show Game section
    var gameSection = document.getElementById("gameSection");
    gameSection.className = "visible";
};

createAccount = function(userName, password){

    var payload = {
        userName: userName,
        password: password
    };

    jQuery.ajax ({
        url: "api/psr/createAccount",
        type: "POST",
        data: JSON.stringify(payload),
        contentType: "application/json; charset=utf-8",
        success: function(data, status){
            if (status === "success"){
                switchToLogin();
            } else {
                updateAccountStatus("Failed to create account!")
            }
        },
        error: function (textStatus, errorThrown) {
            if (textStatus.status == "405"){
                updateAccountStatus("user already exists");
            } else {
                updateAccountStatus(textStatus + errorThrown);
            }
        }
    });
};

updateAccountStatus = function(message){
    var userInfo = document.getElementById("accountStatus");
    accountStatus.innerHTML = message;
};

switchToLogin = function(){
    updateAccountStatus("");

    var submitButton = document.getElementById("submitButton");
    submitButton.value = "Login";
    isLogin = true;
    goToCreateButton.style.visibility = "visible";
};

changeModeToCreate = function(){
    submitButton.value = "Create Account";
    isLogin = false;
    var goToCreateButton = document.getElementById("goToCreateButton");
    goToCreateButton.style.visibility = "hidden";
};

choiceMade = function(choice, element) {
    if (userActionsDisabled === true){
        return;
    }

    lockUserInteraction(true);

    element.className = "image-selected";

    userActionsDisabled = true;

    var payload = {
        userName: currentUser,
        move: choice
    };

    jQuery.ajax ({
        url: "api/psr/userPlay",
        type: "POST",
        data: JSON.stringify(payload),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data, status) {
            if (status === "success") {
                var gameResult = data.result;
                var opponentMove = data.opponentMove;
                var winningStreaks = data.winningStreaks;
                updateGameResult(gameResult, opponentMove, winningStreaks);
            } else {
                // Set game info
            }
        },
        error: function (textStatus, errorThrown) {
            // Set game info
        }
    });
};

lockUserInteraction = function(lock) {

    userActionsDisabled = lock;

    var paperImage = document.getElementById("paperImg");
    var scissorsImage = document.getElementById("scissorsImg");
    var rockImage = document.getElementById("rockImg");

    var className = "";
    if (lock === false) {
        className = "user-hover";
    }
    $(paperImage).parent().disabled = lock;

    paperImage.className = className;
    scissorsImage.className = className;
    rockImage.className = className;
};

updateGameResult = function(gameResult, opponentMove, winningStreaks) {
    clearInterval(countdownTimer);

    var image = psrImages.unknown;
    if (opponentMove === "P") {
        image = psrImages.paper;
    } else if (opponentMove === "S") {
        image = psrImages.scissors;
    } else if (opponentMove === "R") {
        image = psrImages.rock;
    }

    var opponentImage = document.getElementById("opponentImg");
    opponentImage.src = image;

    // Update user info with game result
    var userInfo = document.getElementById("gameInfo");
    var userText = "It's a tie";
    if (gameResult < 0) {
        userText = "You lost !!!"
    } else if (gameResult > 0) {
        userText = "You won !!!"
    }
    userInfo.innerHTML = userText;

    var nextGameSection = document.getElementById("nextGameSection");
    nextGameSection.className = "visible";

    userWinningStreaks = winningStreaks;
};

startNewGame = function(){
    var nextGameSection = document.getElementById("nextGameSection");
    nextGameSection.className = "hidden";

    var opponentImage = document.getElementById("opponentImg");
    opponentImage.src = psrImages.unknown;

    clearInterval(countdownTimer);
    counter = getCountdownTime();
    countdownTimer = setInterval (countdownFunction, 1000);

    lockUserInteraction(false);
};

getCountdownTime = function(){
    countdownTime = 10 - userWinningStreaks;

    return countdownTime;
};